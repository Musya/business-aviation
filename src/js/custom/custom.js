/*----------------------------------------
 FOR SVG SPRITE
 ----------------------------------------*/

// svg4everybody({});

// var click = document.querySelector('.click-me');

// click.addEventListener('click', function () {
//   alert('ok');
// });

/*----------------------------------------
 Language Select
 ----------------------------------------*/


$('.language-select').click(function () {
	$(this).toggleClass('open');
});

$('.language-select li').click(function() {
	var setLang = $('.language-select').data('location'),
		dataLangSelect = $(this).data('lang');
	$('.language-select').data('location', dataLangSelect);
	$('.language-select li').removeClass('active');
	$(this).toggleClass('active');
});

/*----------------------------------------
 Modal form
 ----------------------------------------*/

// $(".js-form-button").on("click", function () {
// 	$(".modal-form").toggleClass("form-open");
// 	$("body").toggleClass("body-overflow");
// });

// $('.close-form').on('click', function () {
// 	$(".modal-form").removeClass("form-open");
// 	$("body").removeClass("body-overflow");
// });

$(document).ready(function () {
	$(".js-btn-form").click(function () {
		$("#myModal").modal('show');
	});
});




/*----------------------------------------
 Swiper slider
 ----------------------------------------*/

// var mySwiper = app.swiper('.swiper-container', {
// 	speed: 700,
// 	spaceBetween: 100
// });   
